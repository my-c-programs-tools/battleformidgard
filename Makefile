NAME		=	sta

INTRO_DIR	=	Introduction

INTRO_NAME	=	intro

BATTLE_DIR	=	Battle

BATTLE_NAME	=	battle

FINAL_DIR	=	Final

FINAL_NAME	=	final

RM		=	rm -f

all:			intro battle final

intro:
			$(MAKE) -C $(INTRO_DIR) && cp $(INTRO_DIR)/$(NAME) $(INTRO_NAME)

battle:
			$(MAKE) -C $(BATTLE_DIR) && cp $(BATTLE_DIR)/$(NAME) $(BATTLE_NAME)

final:
			$(MAKE) -C $(FINAL_DIR) && cp $(FINAL_DIR)/$(NAME) $(FINAL_NAME)

intro_clean:
			cd $(INTRO_DIR) && $(MAKE) clean

battle_clean:
			cd $(BATTLE_DIR) && $(MAKE) clean

final_clean:
			cd $(FINAL_DIR) && $(MAKE) clean

clean:			intro_clean battle_clean final_clean

intro_fclean:
			cd $(INTRO_DIR) && $(MAKE) fclean

battle_fclean:
			cd $(BATTLE_DIR) && $(MAKE) fclean

final_fclean:
			cd $(FINAL_DIR) && $(MAKE) fclean

fclean:			clean intro_fclean battle_fclean final_fclean
			$(RM) $(NAME)
			$(RM) $(INTRO_NAME)
			$(RM) $(BATTLE_NAME)
			$(RM) $(FINAL_NAME)

re:			fclean all

.PHONY:			all intro battle final intro_clean battle_clean final_clean intro_fclean battle_fclean final_fclean re
