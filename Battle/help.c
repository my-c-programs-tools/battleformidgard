#include <stdlib.h>
#include "sta.h"

static void     display_command(char *name, char *man)
{
  if (checking(CHECK) == ENABLE)
    my_display(CHECKED_BOX, NULL, BOLD);
  my_display(name, YELLOW, BOLD);
  my_putstr("--\t");
  my_display(man, NULL, BOLD);
}

static void	oob_help()
{
  display_command(FIGHT_CMD_NAME, FIGHT_CMD_MAN);
  display_command(TEAM_CMD_NAME, TEAM_CMD_MAN);
  display_command(CHOSE_CMD_NAME, CHOSE_CMD_MAN);
}

static void	ib_help(t_player *player)
{
  display_command(CATCH_CMD_NAME, CATCH_CMD_MAN);
  display_command(RNWAY_CMD_NAME, RNWAY_CMD_MAN);
  if (player->team->nb_elem)
    {
      display_command(SLASH_CMD_NAME, SLASH_CMD_MAN);
      display_command(FIRE_CMD_NAME, FIRE_CMD_MAN);
      display_command(GAMBLE_CMD_NAME, GAMBLE_CMD_MAN);
      display_command(REST_CMD_NAME, REST_CMD_MAN);
    }
}

void     display_help(t_player *player)
{
  checking(ENABLE);
  servant_speaking();
  my_display(HELP_CMD, YELLOW, BOLD);
  my_display(player->name, CYAN, BOLD);
  my_display(" :\n\n", YELLOW, BOLD);
  if (current_ennemy(GET_NMY))
    ib_help(player);
  else
    oob_help();
  display_command(EXIT_CMD_NAME, EXIT_CMD_MAN);
  display_command(HELP_CMD_NAME, HELP_CMD_MAN);
  my_putstr("\n");
  checking(DISABLE);
}
