#include <stdlib.h>
#include "sta.h"

static t_commands	g_oob_cmds[] =
  {
    {"f", "fight", &launch_fight},
    {"t", "team", &team_info},
    {"s", "select", &ask_chosen_one},
    {NULL, NULL, NULL}
  };

static t_commands	g_ib_cmds[] =
  {
    {"s", "slash", &slash},
    {"f", "fire", &fire},
    {"g", "gamble", &gamble},
    {"r", "rest", &rest},
    {"i", "infos", &fight_infos},
    {NULL, NULL, NULL}
  };

static int	oob_menu(char *cmd, t_player *player)
{
  int		n;

  n = 0;
  while (g_oob_cmds[n].short_name
	 && my_strcasecmp(g_oob_cmds[n].short_name, cmd)
	 && my_strcasecmp(g_oob_cmds[n].long_name, cmd))
    ++n;
  if (g_oob_cmds[n].short_name)
    {
      if (n == 2 && player->team->nb_elem == 0)
	{
	  servant_speaking();
	  my_display(NO_TEAM, RED, BOLD);
	  return (TRUE);
	}
      return (g_oob_cmds[n].func(player));
    }
  return (BAD_USER_CMD);
}

static int      ib_menu(char *cmd, t_player *player)
{
  int		n;

  if (!my_strcasecmp("c", cmd) || !my_strcasecmp("catch", cmd))
    return (catch(player));
  if (!my_strcasecmp("q", cmd) || !my_strcasecmp("quit", cmd))
    return (runaway(player));
  n = 0;
  while (g_ib_cmds[n].short_name
	 && my_strcasecmp(g_ib_cmds[n].short_name, cmd)
	 && my_strcasecmp(g_ib_cmds[n].long_name, cmd))
    ++n;
  if (g_ib_cmds[n].short_name)
    {
      if (!player->team->nb_elem)
	return (BAD_USER_CMD);
      return (g_ib_cmds[n].func(player->chosen_one, current_ennemy(GET_NMY)));
    }
  return (BAD_USER_CMD);
}

void		ia_attack(t_creature *sender, t_creature *receiver)
{
  static int	ia_turn = FALSE;
  int		action;

  if (sender == NULL && receiver == NULL)
    ia_turn = FALSE;
  else if (sender->wild)
    {
      if (ia_turn)
	{
	  action = rand() % 4;
	  if (action == 3)
	    ia_turn = FALSE;
	  g_ib_cmds[action].func(sender, receiver);
	}
      else
	ia_turn = TRUE;
    }
}

int	dispatch_menu(char *cmd, t_player *player)
{
  int	ret;

  if (current_ennemy(GET_NMY))
    {
      ret = ib_menu(cmd, player);
      check_alive(player);
      if (!current_ennemy(GET_NMY) && player->chosen_one)
	heal_creature(player->chosen_one);
      return (ret);
    }
  else
    return (oob_menu(cmd, player));
}
