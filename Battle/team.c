#include <stdlib.h>
#include "sta.h"

static void	add_creature_to_team_cont(t_player *player,
					  t_team_ctn *team_ctn)
{
  t_team_ctn	*tmp;

  if (player->team->first == NULL)
    {
      player->team->first = team_ctn;
      player->team->last = team_ctn;
      player->team->first->next = NULL;
      player->team->first->prev = NULL;
    }
  else
    {
      tmp = player->team->last;
      tmp->next = team_ctn;
      team_ctn->prev = tmp;
      player->team->last = team_ctn;
    }
  player->team->nb_elem += 1;
}

int		add_creature_to_team(t_player *player,
				     t_creature *new_creature)
{
  t_team_ctn	*team_ctn;

  if ((team_ctn = malloc(sizeof(*team_ctn))) == NULL)
    return (my_int_error(MALLOC_ERROR, ERROR));
  up_creature(new_creature);
  heal_creature(new_creature);
  team_ctn->creature = new_creature;
  team_ctn->prev = NULL;
  team_ctn->next = NULL;
  add_creature_to_team_cont(player, team_ctn);
  return (SUCCESS);
}

void		del_creature_from_team(t_player *player,
				       t_creature *creature)
{
  t_team_ctn	*tmp;

  tmp = player->team->first;
  while (tmp && tmp->creature != creature)
    tmp = tmp->next;
  if (tmp)
    {
      if (tmp->prev)
	tmp->prev->next = tmp->next;
      else
	player->team->first = tmp->next;
      if (tmp->next)
	tmp->next->prev = tmp->prev;
      else
	player->team->last = tmp->prev;
      if (tmp->creature)
	delete_creature(tmp->creature);
      free(tmp);
      player->team->nb_elem -= 1;
    }
}

int		team_info(t_player *player)
{
  int		i;
  t_team_ctn	*tmp;

  servant_speaking();
  my_putstr(TROUP_REVIEW);
  my_display(player->name, CYAN, BOLD);
  my_putstr(" :\n\n");
  i = 0;
  tmp = player->team->first;
  while (tmp)
    {
      my_display(" [", BLUE, NULL);
      my_display_nbr(i, YELLOW, BOLD);
      my_display("] : ", BLUE, NULL);
      display_creature_info(tmp->creature);
      tmp = tmp->next;
      ++i;
    }
  return (TRUE);
}

void		creature_choose(t_player *player, int index)
{
  t_team_ctn	*tmp;

  tmp = player->team->first;
  while (tmp && index > 0)
    {
      tmp = tmp->next;
      --index;
    }
  player->chosen_one = tmp->creature;
  servant_speaking();
  my_display(SELECT_CREATURE, GREEN, BOLD);
  my_display(tmp->creature->name, PURPLE, BOLD);
  my_putstr("\n\n\t");
  display_creature_info(tmp->creature);
}
