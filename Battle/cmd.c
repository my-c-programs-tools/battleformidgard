#include <stdlib.h>
#include "sta.h"

static int	treat_cmd(char *cmd, t_player *player, int *invalid_cmd)
{
  int		done;

  if (!my_strcasecmp("h", cmd)
      || !my_strcasecmp("help", cmd))
    {
      *invalid_cmd = 0;
      display_help(player);
      return (NO_EFFECT_CMD);
    }
  done = dispatch_menu(cmd, player);
  if (done == BAD_USER_CMD)
    {
      servant_speaking();
      my_display(INVALID_CMD, RED, BOLD);
      my_display(player->name, CYAN, BOLD);
      my_display(" !\n\n", RED, BOLD);
      *invalid_cmd += 1;
    }
  else if (done != ERROR)
    *invalid_cmd = 0;
  if (*invalid_cmd == 10)
    my_display(ARMY_GIVE_UP, RED, BOLD);
  return (done);
}

static int      detect_leaving(char *cmd)
{
  int		ret;

  if ((!my_strcasecmp(cmd, "exit") || !my_strcasecmp(cmd, "e"))
      && (ret = ask_leaving()))
    return (!ret);
  return (TRUE);
}

void		check_alive(t_player *player)
{
  t_creature	*ennemy;

  ennemy = current_ennemy(GET_NMY);
  if (player->chosen_one && player->chosen_one->pv <= 0)
    {
      my_display(player->chosen_one->name, PURPLE, BOLD);
      my_display(DEAD_CREATURE, PURPLE, BOLD);
      servant_speaking();
      my_display(FIGHT_FAILURE, RED, BOLD);
      del_creature_from_team(player, player->chosen_one);
      player->chosen_one = NULL;
      current_ennemy(DELETE_NMY);
    }
  else if (ennemy && ennemy->pv <= 0)
    {
      my_display(ennemy->name, PURPLE, BOLD);
      my_display(DEAD_CREATURE, PURPLE, BOLD);
      servant_speaking();
      my_display(FIGHT_SUCCESS, GREEN, BOLD);
      heal_creature(player->chosen_one);
      current_ennemy(DELETE_NMY);
    }
}

int	main_loop(t_player *player)
{
  int   cmd_ret;
  char	*cmd;
  int	invalid_cmd;

  cmd_ret = TRUE;
  invalid_cmd = 0;
  while (cmd_ret && invalid_cmd < 10)
    {
      if ((cmd = read_line(NULL)) != NULL)
	{
	  cmd_ret = detect_leaving(cmd);
	  if (cmd_ret && cmd_ret != ERROR)
	    cmd_ret = treat_cmd(cmd, player, &invalid_cmd);
	  free(cmd);
	}
      if (cmd_ret != NO_EFFECT_CMD &&
	  current_ennemy(GET_NMY) && player->chosen_one)
	{
	  ia_attack(current_ennemy(GET_NMY), player->chosen_one);
	  check_alive(player);
	}
      if (cmd == NULL || cmd_ret == ERROR)
	return (ERROR);
    }
  return (SUCCESS);
}
