#include <stdlib.h>
#include "sta.h"

void	display_attack(char *sender, char *receiver, char *msg)
{
  servant_speaking();
  my_display(sender, PURPLE, BOLD);
  my_display(msg, NULL, NULL);
  my_display(receiver, PURPLE, BOLD);
  my_putstr("\n\n");
}

int	fight_infos(t_creature *chosen, t_creature *ennemy)
{
  servant_speaking();
  my_display(CHMP_INFO, NULL, NULL);
  display_creature_info(chosen);
  servant_speaking();
  my_display(ENNEMY_INFO, NULL, NULL);
  display_creature_info(ennemy);
  return (NO_EFFECT_CMD);
}
