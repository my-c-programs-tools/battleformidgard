#ifndef			_STA_H_
# define		_STA_H_

# include		"../common/common.h"

/*
** Structures
*/

typedef struct		s_team_ctn
{
  t_creature		*creature;
  struct s_team_ctn	*next;
  struct s_team_ctn	*prev;
}			t_team_ctn;

typedef struct		s_team
{
  t_team_ctn		*first;
  t_team_ctn		*last;
  int			nb_elem;
}			t_team;

typedef struct		s_player
{
  char			*name;
  t_team		*team;
  int			magicbox;
  t_creature		*chosen_one;
}			t_player;

/*
** Ressources
*/

# define		IA_TURN		0
# define		PLAYER_TURN	1

/*
** Configuration
**
** - For Fonts and Colors, set to 'NULL' if you
**   want the shell's default values
**
** - Don't use NULL for other configurations (such as
**   messages, prompt, etc...) just use an empty
**   string ("")
**
** - Don't use 0 for integer configurations,
**   keep only strictly positive integers
**
*/

# define		DFLT_MGKBX	5

/*
** Messages
*/

# define		HELP_CMD	"Here are the available orders "

# define		HELP_CMD_NAME	"[h]elp\t"
# define		EXIT_CMD_NAME	"[e]xit\t"

# define		FIGHT_CMD_NAME	"[f]ight\t"
# define		TEAM_CMD_NAME	"[t]eam\t"
# define		CHOSE_CMD_NAME	"[s]elect\t"

# define		RNWAY_CMD_NAME	"[q]uit\t"
# define		CATCH_CMD_NAME	"[c]atch\t"
# define		SLASH_CMD_NAME	"[s]lash\t"
# define		FIRE_CMD_NAME	"[f]ire\t"
# define		GAMBLE_CMD_NAME	"[g]amble\t"
# define		REST_CMD_NAME	"[r]est\t"

# define		HELP_CMD_MAN	"I'll yell the available orders at you\
, like I'm doing right now !\n"
# define		EXIT_CMD_MAN	"Definitely give up on Dark Lord life \
and maybe build a little farm and found a family\n"

# define		FIGHT_CMD_MAN	"Pretty explicit on my opinion\n"
# define		TEAM_CMD_MAN	"Review the troops\n"
# define		CHOSE_CMD_MAN	"Choose your fighting champion\n"

# define		RNWAY_CMD_MAN	"Leave (courageously) the battlefield\n"
# define		CATCH_CMD_MAN	"Try to catch the beast !\n"
# define		SLASH_CMD_MAN	"Order your creature to slash the \
ennemy (15dmg / -3 pm)\n"
# define		FIRE_CMD_MAN	"Order your creature to burn the \
ennemy (30dmg / -7 pm)\n"
# define		GAMBLE_CMD_MAN	"Order your creature to gamble on \
damages (0-20dmg / random target)\n"
# define		REST_CMD_MAN	"Order your creature to rest (+10pm / \
-1 turn)\n"

# define		INVALID_CMD	"We can't execute that order "

# define		ASK_CHOSE	"What creature do you want to select ?"
# define		ASK_CHOSE_MAN	"numbers only, between 0 and last \
member of team"
# define		ASK_CHOSE_ERROR	"Wrong answer, please try again"

# define		NO_MGKBX	"We don't have any magicbox for \
catching "
# define		CATCH_FAILURE	"We failed at catching "
# define		CATCH_SUCCESS	"We successfully catched "

# define		RUNNING_AWAY	"Let's leave right away !\n\n"

# define		NO_TEAM		"You have no troops my Lord\n\n"
# define		SELECT_CREATURE	"Very well my Lord, you champion is \
now "

# define		SLASH_MSG	" slashed the ennemy and inflicted 15 \
damages to "
# define		FIRE_MSG	" burned the ennemy and inflicted 15 \
damages to "
# define		GAMBLE_MSG	" gambled and inflicted "
# define		GAMBLE_DMG	" damages to "
# define		REST_MSG	" rested and regained 10PM\n\n"

# define		NOT_ENOUGH_PM	" doesn't have enough PM !\n"

# define		DEAD_CREATURE	" is dead !\n"
# define		FIGHT_FAILURE	"We have to run away my Lord, our \
champion is dead\n\n"
# define		FIGHT_SUCCESS	"We have won the battle my Lord \
!\n\n"

# define		CHMP_INFO	"Our champion :\n\n\t"
# define		ENNEMY_INFO	"The ennemy :\n\n\t"

# define		TROUP_REVIEW	"Here is the review of your troups "

/*
** ask.c
*/
int			ask_chosen_one(t_player*);

/*
** catch.c
*/
int			catch(t_player*);

/*
** cmd.c
*/
int			main_loop(t_player*);
void			check_alive(t_player*);

/*
** display_fight.c
*/
void			display_attack(char*, char*, char*);
int			fight_infos(t_creature*, t_creature*);

/*
** fight.c
*/
int			slash(t_creature*, t_creature*);
int			fire(t_creature*, t_creature*);
int			gamble(t_creature*, t_creature*);
int			rest(t_creature*, t_creature*);
int			launch_fight(t_player*);

/*
** help.c
*/
void			display_help(t_player*);

/*
** menus.c
*/
void			ia_attack(t_creature*, t_creature*);
int			dispatch_menu(char*, t_player*);

/*
** player.c
*/
t_player		*init_player(t_args*);
void			free_player(t_player*);

/*
** runaway.c
*/
int			runaway(t_player*);

/*
** team.c
*/
int			add_creature_to_team(t_player*, t_creature*);
void			del_creature_from_team(t_player*, t_creature*);
int			team_info(t_player*);
void			creature_choose(t_player*, int);

#endif			/* !_STA_H_ */
