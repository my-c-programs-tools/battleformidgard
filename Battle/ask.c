#include <stdlib.h>
#include "sta.h"

static int	check_index(char *cmd, t_player *player)
{
  int		i;

  i = 0;
  while (cmd[i] && cmd[i] >= '0' && cmd[i] <= '9')
    ++i;
  if (cmd[i])
    return (-1);
  i = my_getnbr(cmd);
  if (i >= 0 && i < player->team->nb_elem)
    return (i);
  return (-1);
}

int	ask_chosen_one(t_player *player)
{
  int	ret;
  int	res;
  char	*cmd;

  ret = 0;
  res = -1;
  team_info(player);
  display_instruction(ASK_CHOSE, ASK_CHOSE_MAN);
  while (ret != ERROR && res == -1)
    {
      if ((cmd = read_line("team")) == NULL)
	ret = ERROR;
      else if ((res = check_index(cmd, player)) == -1)
	display_error_instruction(ASK_CHOSE_ERROR, ASK_CHOSE_MAN);
      free(cmd);
    }
  if (res != -1)
    creature_choose(player, res);
  return ((ret == ERROR) ? (ret) : (TRUE));
}
