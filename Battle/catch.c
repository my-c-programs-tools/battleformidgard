#include <stdlib.h>
#include "sta.h"

static void	display_catch_msg(char *msg, t_creature *ennemy,
				  char *name, int win)
{
  servant_speaking();
  my_display(msg, (win) ? (GREEN) : (RED), BOLD);
  my_display(ennemy->name, PURPLE, BOLD);
  my_putstr(" ");
  my_display(name, CYAN, BOLD);
  my_putstr("\n\n");
  if (win)
    display_creature_info(ennemy);
}

int		catch(t_player *player)
{
  double	success;
  t_creature	*ennemy;

  ennemy = current_ennemy(GET_NMY);
  success = (rand() % 100) * (2.0 - (double)(ennemy->pv / ennemy->pvmax));
  if (player->magicbox && success >= 66.0)
    {
      display_catch_msg(CATCH_SUCCESS, ennemy, player->name, TRUE);
      add_creature_to_team(player, ennemy);
      current_ennemy(RESET_NMY);
    }
  else
    {
      display_catch_msg((player->magicbox) ? (CATCH_FAILURE) : (NO_MGKBX),
			ennemy, player->name, FALSE);
      if (!player->chosen_one)
	{
	  servant_speaking();
	  my_display(RUNNING_AWAY, RED, BOLD);
	  current_ennemy(DELETE_NMY);
	}
    }
  if (player->magicbox > 0)
    player->magicbox -= 1;
  return (TRUE);
}
