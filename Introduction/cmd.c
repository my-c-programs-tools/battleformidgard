#include <stdlib.h>
#include "sta.h"

static t_commands	g_cmds[] =
  {
    {"q", "quit", &runaway},
    {"c", "catch", &catch},
    {NULL, NULL, NULL}
  };

static int	treat_cmd(char *cmd, t_player *player, int *invalid_cmd)
{
  int		n;

  if (!my_strcasecmp("h", cmd)
      || !my_strcasecmp("help", cmd))
    {
      display_help(player);
      return (FALSE);
    }
  n = 0;
  while (g_cmds[n].short_name
	 && my_strcasecmp(g_cmds[n].short_name, cmd)
	 && my_strcasecmp(g_cmds[n].long_name, cmd))
    ++n;
  if (g_cmds[n].short_name)
    {
      *invalid_cmd = 0;
      return (g_cmds[n].func(player));
    }
  else
    *invalid_cmd += 1;
  servant_speaking();
  my_display(INVALID_CMD, RED, BOLD);
  my_display(player->name, CYAN, BOLD);
  my_display(" !\n\n", RED, BOLD);
  return (FALSE);
}

static int	detect_leaving(char *cmd)
{
  int		ret;

  if ((!my_strcasecmp(cmd, "exit") || !my_strcasecmp(cmd, "e"))
      && (ret = ask_leaving()))
    return (ret);
  return (FALSE);
}

int	main_loop(t_player *player)
{
  int	done;
  char	*cmd;
  int	invalid_cmd;

  done = FALSE;
  invalid_cmd = 0;
  while (!done && invalid_cmd < 10)
    {
      if (!current_ennemy(GET_NMY))
	current_ennemy(CREATE_NMY);
      if ((cmd = read_line(NULL)) != NULL)
	{
	  done = detect_leaving(cmd);
	  if (!done && done != ERROR)
	    done = treat_cmd(cmd, player, &invalid_cmd);
	  free(cmd);
	}
      if (cmd == NULL || done == ERROR)
	return (ERROR);
    }
  if (invalid_cmd == 10)
    my_display(ARMY_GIVE_UP, RED, BOLD);
  return (SUCCESS);
}
