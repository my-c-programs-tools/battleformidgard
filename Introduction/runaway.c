#include "sta.h"

int	runaway(t_player *player)
{
  servant_speaking();
  my_putstr("Yes ");
  my_display(player->name, CYAN, BOLD);
  my_putstr(RUNNING_AWAY);
  current_ennemy(DELETE_NMY);
  return (FALSE);
}
