#include <stdlib.h>
#include "sta.h"

static void     display_command(char *name, char *man)
{
  if (checking(CHECK) == ENABLE)
    my_display(CHECKED_BOX, NULL, BOLD);
  my_display(name, YELLOW, BOLD);
  my_putstr("--\t");
  my_display(man, NULL, BOLD);
}

void     display_help(t_player *player)
{
  checking(ENABLE);
  servant_speaking();
  my_display(HELP_CMD, YELLOW, BOLD);
  my_display(player->name, CYAN, BOLD);
  my_display(" :\n\n", YELLOW, BOLD);
  display_command(CATCH_CMD_NAME, CATCH_CMD_MAN);
  display_command(RNWAY_CMD_NAME, RNWAY_CMD_MAN);
  display_command(EXIT_CMD_NAME, EXIT_CMD_MAN);
  display_command(HELP_CMD_NAME, HELP_CMD_MAN);
  my_putstr("\n");
  checking(DISABLE);
}
