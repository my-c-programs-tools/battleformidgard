#include <stdlib.h>
#include "sta.h"

int	catch(t_player *player)
{
  int	success;

  success = rand() % 3;
  servant_speaking();
  if (success == 2)
    {
      my_display(CATCH_SUCCESS, GREEN, NULL);
      add_creature_to_team(player, current_ennemy(GET_NMY));
      my_display(current_ennemy(GET_NMY)->name, PURPLE, BOLD);
      my_putstr(" ");
      current_ennemy(RESET_NMY);
    }
  else
    {
      my_display(CATCH_FAILURE, RED, NULL);
      my_display(current_ennemy(GET_NMY)->name, PURPLE, BOLD);
      my_putstr(" ");
      current_ennemy(DELETE_NMY);
    }
  my_display(player->name, CYAN, BOLD);
  my_putstr("\n\n");
  if (success == 2)
    display_creature_info(player->team->last->creature);
  return ((success == 2) ? (TRUE) : (FALSE));
}
