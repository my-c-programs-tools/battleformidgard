#ifndef			_STA_H_
# define		_STA_H_

# include		"../common/common.h"

/*
** Structures
*/

typedef struct		s_team_ctn
{
  t_creature		*creature;
  struct s_team_ctn	*next;
  struct s_team_ctn	*prev;
}			t_team_ctn;

typedef struct		s_team
{
  t_team_ctn		*first;
  t_team_ctn		*last;
  int			nb_elem;
}			t_team;

typedef struct		s_player
{
  char			*name;
  t_team		*team;
}			t_player;

/*
** Messages
*/

# define		HELP_CMD	"Here are the available orders "

# define		RNWAY_CMD_NAME	"[q]uit\t"
# define		CATCH_CMD_NAME	"[c]atch\t"
# define		HELP_CMD_NAME	"[h]elp\t"
# define		EXIT_CMD_NAME	"[e]xit\t"

# define	        RNWAY_CMD_MAN	"Leave (courageously) the battlefield\n"
# define		CATCH_CMD_MAN	"Try to catch the beast !\n"
# define		HELP_CMD_MAN	"I'll yell the available orders at you\
, like I'm doing right now !\n"
# define		EXIT_CMD_MAN	"Definitely give up on Dark Lord life \
and maybe build a little farm and found a family\n"

# define		INVALID_CMD	"We can't execute that order "

# define	        CATCH_FAILURE	"We have to run. Fast ! We failed at \
catching "
# define	        CATCH_SUCCESS	"We successfully catched "

# define		RUNNING_AWAY	"Let's leave right away !\n\n"

/*
** catch.c
*/
int			catch(t_player*);

/*
** cmd.c
*/
int			main_loop(t_player*);

/*
** help.c
*/
void		        display_help(t_player*);

/*
** player.c
*/
t_player		*init_player(t_args*);
void			free_player(t_player*);

/*
** runaway.c
*/
int			runaway(t_player*);

/*
** team.c
*/
int			add_creature_to_team(t_player*, t_creature*);
void			del_creature_from_team(t_player*, t_creature*);

#endif			/* !_STA_H_ */
