#include <stdlib.h>
#include "sta.h"

static int	add_team_to_player(t_player *player)
{
  t_team	*new_team;

  if ((new_team = malloc(sizeof(t_team))) == NULL)
    return (my_int_error(MALLOC_ERROR, ERROR));
  new_team->first = NULL;
  new_team->last = NULL;
  new_team->nb_elem = 0;
  player->team = new_team;
  return (SUCCESS);
}


t_player	*init_player(t_args *args)
{
  t_player	*new_player;

  if ((new_player = malloc(sizeof(t_player))) == NULL)
    return (my_null_error(MALLOC_ERROR));
  if ((new_player->name = my_strdup(args->name)) == NULL
      || add_team_to_player(new_player) == ERROR)
    {
      if (new_player->name)
	free(new_player->name);
      free(new_player);
      return (NULL);
    }
  return (new_player);
}

void		free_player(t_player *player)
{
  t_team_ctn	*tmp;

  free(player->name);
  while (player->team->first != NULL)
    {
      tmp = player->team->first;
      player->team->first = player->team->first->next;
      delete_creature(tmp->creature);
      free(tmp);
    }
  free(player->team);
  free(player);
}
