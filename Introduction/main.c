#include <stdlib.h>
#include <time.h>
#include "sta.h"

static void	init_args(t_args *options)
{
  options->name = NULL;
}

static int     fill_default_args(t_args *options)
{
  if ((options->name = my_strdup(DFLT_PLYR_NAME)) == NULL)
    return (ERROR);
  return (SUCCESS);
}

static void     delete_options(t_args *options)
{
  free(options->name);
}

static void	greetings()
{
  servant_speaking();
  my_display(SRVT_WELCOME, GREEN, BOLD);
  my_display(SERVANT_NAME, CYAN, UNDERLINED);
  my_display(SRVT_GOAL, GREEN, BOLD);
}

int		main(int argc, char **argv)
{
  t_args	options;
  t_player	*player;
  int		ret;

  srand(time(NULL));
  init_args(&options);
  if (argc > 1)
    ret = get_args(&options, argc, argv);
  else
    ret = fill_default_args(&options);
  if (ret == ERROR)
    return (-1);
  player = init_player(&options);
  delete_options(&options);
  if (!player)
    return (-1);
  display_prompt(player->name, TRUE);
  greetings();
  my_putstr(CLEAR);
  main_loop(player);
  free_player(player);
  if (current_ennemy(GET_NMY))
    current_ennemy(DELETE_NMY);
  return (0);
}
