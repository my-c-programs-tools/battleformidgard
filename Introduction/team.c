#include <stdlib.h>
#include "sta.h"

static void	add_creature_to_team_cont(t_player *player,
					  t_team_ctn *team_ctn)
{
  t_team_ctn	*tmp;

  if (player->team->first == NULL)
    {
      player->team->first = team_ctn;
      player->team->last = team_ctn;
      player->team->first->next = NULL;
      player->team->first->prev = NULL;
    }
  else
    {
      tmp = player->team->last;
      tmp->next = team_ctn;
      team_ctn->prev = tmp;
      player->team->last = team_ctn;
    }
  player->team->nb_elem += 1;
}

int		add_creature_to_team(t_player *player,
				     t_creature *new_creature)
{
  t_team_ctn	*team_ctn;

  if ((team_ctn = malloc(sizeof(t_team_ctn))) == NULL)
    return (my_int_error(MALLOC_ERROR, ERROR));
  team_ctn->creature = new_creature;
  team_ctn->prev = NULL;
  team_ctn->next = NULL;
  add_creature_to_team_cont(player, team_ctn);
  return (SUCCESS);
}

void		del_creature_from_team(t_player *player,
				       t_creature *creature)
{
  t_team_ctn	*tmp;

  tmp = player->team->first;
  while (tmp && tmp->creature != creature)
    tmp = tmp->next;
  if (tmp)
    {
      if (tmp->prev)
	tmp->prev->next = tmp->next;
      else
	player->team->first = tmp->next;
      if (tmp->next)
	tmp->next->prev = tmp->prev;
      else
	player->team->last = tmp->prev;
      if (tmp->creature)
	delete_creature(tmp->creature);
      free(tmp);
      player->team->nb_elem -= 1;
    }
}
