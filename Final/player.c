#include <stdlib.h>
#include "sta.h"

static int	add_team_to_player(t_player *player)
{
  t_team	*new_team;

  if ((new_team = malloc(sizeof(*new_team))) == NULL)
    return (my_int_error(MALLOC_ERROR, ERROR));
  new_team->first = NULL;
  new_team->last = NULL;
  new_team->nb_elem = 0;
  player->team = new_team;
  return (SUCCESS);
}


t_player	*init_player(t_args *args)
{
  t_player	*new_player;

  if ((new_player = malloc(sizeof(*new_player))) == NULL)
    return (my_null_error(MALLOC_ERROR));
  if ((new_player->name = my_strdup(args->name)) == NULL
      || add_team_to_player(new_player) == ERROR)
    {
      if (new_player->name)
	free(new_player->name);
      free(new_player);
      return (NULL);
    }
  new_player->chosen_one = NULL;
  if ((new_player->bag = malloc(sizeof(t_inventory))) == NULL)
    {
      free(new_player->name);
      free(new_player->team);
      free(new_player);
      return (my_null_error(MALLOC_ERROR));
    }
  new_player->bag->rupees = DFLT_RUPEES;
  new_player->bag->magicbox = DFLT_MGKBX;
  new_player->bag->shrooms = 0;
  new_player->is_shopping = FALSE;
  return (new_player);
}

void		free_player(t_player *player)
{
  t_team_ctn	*tmp;

  free(player->name);
  while (player->team->first != NULL)
    {
      tmp = player->team->first;
      player->team->first = player->team->first->next;
      delete_creature(tmp->creature);
      free(tmp);
    }
  free(player->bag);
  free(player->team);
  free(player);
}

int		use_shroom(t_player *player)
{
  int		rand_value;
  t_creature	*creature;

  creature = player->chosen_one;
  if (creature)
    {
      if (player->bag->shrooms)
	{
	  rand_value = (rand() % 11) + 15;
	  player->bag->shrooms -= 1;
	  creature->pv += ((double)creature->pvmax * (double)(rand_value / 100));
	  if (creature->pv > creature->pvmax)
	    creature->pv = creature->pvmax;
	  display_creature_info(creature);
	}
      else
	my_display(NO_SHROOMS, RED, BOLD);
    }
  else
    {
      servant_speaking();
      my_display(NO_CHOSE, RED, BOLD);
    }
  return (SUCCESS);
}
