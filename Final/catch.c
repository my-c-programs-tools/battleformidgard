#include <stdlib.h>
#include "sta.h"

static void	display_catch_msg(char *msg, t_creature *ennemy,
				  char *name, int win)
{
  servant_speaking();
  my_display(msg, (win) ? (GREEN) : (RED), BOLD);
  my_display(ennemy->name, PURPLE, BOLD);
  my_putstr(" ");
  my_display(name, CYAN, BOLD);
  my_putstr("\n\n");
  if (win)
    display_creature_info(ennemy);
}

int		catch(t_player *player)
{
  t_creature	*ennemy;

  ennemy = current_ennemy(GET_NMY);
  if (player->bag->magicbox &&
      ((rand() % 100) * (2.0 - (double)(ennemy->pv / ennemy->pvmax))) >= 66.0)
    {
      display_catch_msg(CATCH_SUCCESS, ennemy, player->name, TRUE);
      add_creature_to_team(player, ennemy);
      current_ennemy(RESET_NMY);
    }
  else
    {
      display_catch_msg((player->bag->magicbox) ? (CATCH_FAIL) : (NO_MGKBX),
			ennemy, player->name, FALSE);
      if (!player->chosen_one)
	{
	  servant_speaking();
	  my_display(RUNNING_AWAY, RED, BOLD);
	  current_ennemy(DELETE_NMY);
	}
    }
  if (player->bag->magicbox > 0)
    player->bag->magicbox -= 1;
  return (TRUE);
}

void	ennemy_catch(t_creature *receiver)
{
  servant_speaking();
  my_putstr(NMY_CATCH);
  servant_speaking();
  if ((rand() % 3) == 2)
    {
      my_display(NMY_CATCH_SUCC, RED, BOLD);
      receiver->wild = TRUE;
    }
  else
    my_display(NMY_CATCH_FAIL, GREEN, BOLD);
}
