#include <stdlib.h>
#include "sta.h"

int	buy_shroom(t_player *player)
{
  if (player->bag->rupees >= 30)
    {
      player->bag->rupees -= 30;
      player->bag->shrooms += 1;
      my_putstr(YOU_HAVE);
      my_display_nbr(player->bag->shrooms, YELLOW, BOLD);
      my_putstr(SHROOM_NB);
    }
  else
    my_display(NOT_ENOUGH_MNEY, RED, BOLD);
  my_display(YOU_HAVE, YELLOW, BOLD);
  my_display_nbr(player->bag->rupees, YELLOW, BOLD);
  my_display(RUPEES_NB, NULL, NULL);
  return (SUCCESS);
}

int	buy_magicbox(t_player *player)
{
  if (player->bag->rupees >= 90)
    {
      player->bag->rupees -= 90;
      player->bag->magicbox += 1;
      my_putstr(YOU_HAVE);
      my_display_nbr(player->bag->magicbox, YELLOW, BOLD);
      my_putstr(MAGICBOX_NB);
    }
  else
    my_display(NOT_ENOUGH_MNEY, RED, BOLD);
  my_display(YOU_HAVE, YELLOW, BOLD);
  my_display_nbr(player->bag->rupees, YELLOW, BOLD);
  my_display(RUPEES_NB, NULL, NULL);
  return (SUCCESS);
}

int	deactivate_shopping(t_player *player)
{
  player->is_shopping = FALSE;
  return (display_help(player));
}

int	activate_shopping(t_player *player)
{
  player->is_shopping = TRUE;
  return (display_help(player));
}

int	display_inventory(t_player *player)
{
  servant_speaking();
  my_putstr(INVENTORY_RECAP);
  my_display_nbr(player->bag->magicbox, YELLOW, BOLD);
  my_putstr(MAGICBOX_NB);
  my_display_nbr(player->bag->shrooms, YELLOW, BOLD);
  my_putstr(SHROOM_NB);
  my_display_nbr(player->bag->rupees, YELLOW, BOLD);
  my_putstr(RUPEES_NB);
  my_putstr("\n");
  return (SUCCESS);
}
