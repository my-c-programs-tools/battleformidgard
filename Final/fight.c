#include <stdlib.h>
#include "sta.h"

int	slash(t_creature *sender, t_creature *receiver)
{
  if (sender->pm >= 3)
    {
      sender->pm -= 3;
      receiver->pv -= 15;
      display_attack(sender->name, receiver->name, SLASH_MSG);
    }
  else
    {
      servant_speaking();
      my_display(sender->name, PURPLE, BOLD);
      my_display(NOT_ENOUGH_PM, RED, BOLD);
    }
  return (TRUE);
}

int	fire(t_creature *sender, t_creature *receiver)
{
  if (sender->pm >= 3)
    {
      sender->pm -= 7;
      receiver->pv -= 30;
      display_attack(sender->name, receiver->name, FIRE_MSG);
    }
  else
    {
      servant_speaking();
      my_display(sender->name, PURPLE, BOLD);
      my_display(NOT_ENOUGH_PM, RED, BOLD);
    }
  return (TRUE);
}

int	gamble(t_creature *sender, t_creature *receiver)
{
  int	rand_value;
  int	rand_pv;

  rand_pv = (rand() % 21);
  rand_value = (rand() % 100) + 1;
  servant_speaking();
  my_display(sender->name, PURPLE, BOLD);
  my_display(GAMBLE_MSG, NULL, NULL);
  my_display_nbr(rand_pv, YELLOW, BOLD);
  my_display(" to ", NULL, NULL);
  if (rand_value <= 50)
    {
      sender->pv -= rand_pv;
      my_display("himself", PURPLE, BOLD);
    }
  else
    {
      receiver->pv -= rand_pv;
      my_display(receiver->name, PURPLE, BOLD);
    }
  my_putstr("\n\n");
  return (TRUE);
}

int	rest(t_creature *sender, t_creature *receiver)
{
  sender->pm += 10;
  servant_speaking();
  my_display(sender->name, PURPLE, BOLD);
  my_display(REST_MSG, NULL, NULL);
  ia_attack(receiver, sender);
  return (TRUE);
}

int	launch_fight(t_player *player)
{
  my_putstr(CLEAR);
  ia_attack(NULL, NULL);
  if (player->chosen_one == NULL && player->team->first)
    player->chosen_one = player->team->first->creature;
  if (current_ennemy(CREATE_NMY) == NULL)
    return (ERROR);
  return (display_help(player));
}
