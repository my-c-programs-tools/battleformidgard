#include <stdlib.h>
#include "common.h"

int	ask_leaving()
{
  char	*str;
  int	ret;

  display_instruction(ASK_LVNG, ASK_LVNG_MAN);
  str = NULL;
  ret = -1;
  while (ret == -1)
    {
      if ((str = read_line(NULL)) == NULL)
	return (TRUE);
      if (my_strcasecmp("yes", str) == 0 || my_strcasecmp("y", str) == 0)
	ret = TRUE;
      else if (my_strcasecmp("no", str) == 0 || my_strcasecmp("n", str) == 0)
	ret = FALSE;
      else
	display_error_instruction(ASK_LVNG_ERROR, ASK_LVNG_MAN);
      free(str);
    }
  return (ret);
}
