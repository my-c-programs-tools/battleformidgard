#include <stdlib.h>
#include "common.h"

static void	display_check(int current, int base, int prct)
{
  if (prct)
    {
      if ((current * 100) < (base * 33))
	my_display(LOWER_BOX, RED, BOLD);
      else if ((current * 100) < (base * 66))
	my_display(EQUAL_BOX, YELLOW, BOLD);
      else
	my_display(UPPER_BOX, GREEN, BOLD);
    }
  else
    {
      if (current < base)
	my_display(LOWER_BOX, RED, BOLD);
      else if (current > base)
	my_display(UPPER_BOX, GREEN, BOLD);
      else
	my_display(EQUAL_BOX, YELLOW, BOLD);
    }
}

static void	display_value(int current, int base, int prct)
{
  if (prct)
    {
      if ((current * 100) < (base * 33))
	my_display_nbr(current, RED, NULL);
      else if ((current * 100) < (base * 66))
	my_display_nbr(current, YELLOW, NULL);
      else
	my_display_nbr(current, GREEN, NULL);
    }
  else
    {
      if (current < base)
	my_display_nbr(current, RED, NULL);
      else if (current > base)
	my_display_nbr(current, GREEN, NULL);
      else
	my_display_nbr(current, YELLOW, NULL);
    }
}

static void	display_info(char *stat, int current, int base, int prct)
{
  display_check(current, base, prct);
  set_color(BLUE);
  my_putstr(stat);
  my_putstr("\t: ");
  display_value(current, base, prct);
  my_putstr(" (");
  my_display_nbr(base, PURPLE, NULL);
  my_putstr(")\n");
  reset_color();
}

void	display_creature_info(t_creature *creature)
{
  checking(ENABLE);
  my_display(creature->name, PURPLE, BOLD);
  my_putstr("\n\n");
  display_info(CREATURE_LVL, creature->lvl, 1, FALSE);
  display_info(CREATURE_PV, creature->pv, creature->pvmax, TRUE);
  display_info(CREATURE_PM, creature->pm, creature->pmmax, TRUE);
  my_putstr("\n");
  checking(DISABLE);
}
