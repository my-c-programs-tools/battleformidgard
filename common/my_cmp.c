int	my_strcmp(char *s1, char *s2)
{
  while (*s1 && *s2 && *s1 == *s2)
    {
      ++s1;
      ++s2;
    }
  return (*s1 - *s2);
}

int	my_strncmp(char *s1, char *s2, int n)
{
  int	i;

  i = 0;
  while (s1[i] && s2[i] && s1[i] == s2[i] && i < (n - 1))
    ++i;
  if (s1[i] < s2[i])
    return (-1);
  return ((s1[i] == s2[i]) ? (0) : (1));
}

int	my_strcasecmp(char *s1, char *s2)
{
  int	c1;
  int	c2;

  c1 = 0;
  c2 = 0;
  while (*s1 && *s2 && (c1 == c2))
    {
      c1 = (*s1 >= 'A' && *s1 <= 'Z') ? (*s1 + 32) : (*s1);
      c2 = (*s2 >= 'A' && *s2 <= 'Z') ? (*s2 + 32) : (*s2);
      if (c1 == c2)
	{
	  ++s1;
	  ++s2;
	}
    }
  if (!(*s1) || !(*s2))
    return (*s1 - *s2);
  return (c1 - c2);
}
