#include <stdlib.h>
#include <string.h>
#include "common.h"

static t_creature	g_creatures[] =
  {
    {"Koopa", 1, 10, 10, 20, 20, TRUE},
    {"Bob bomb", 1, 9, 9, 21, 21, TRUE},
    {"Yoshi", 1, 12, 12, 18, 18, TRUE},
    {"Maskas", 1, 8, 8, 22, 22, TRUE},
    {"Kucco", 1, 11, 11, 19, 19, TRUE},
    {NULL, 0, 0, 0, 0, 0, 0}
  };

void		delete_creature(t_creature *creature)
{
  free(creature->name);
  free(creature);
}

t_creature	*get_creature()
{
  int		rnd;
  t_creature	*crea;

  rnd = rand() % NB_CREATURE;
  if ((crea = malloc(sizeof(*crea))) == NULL)
    return (my_null_error(MALLOC_ERROR));
  if ((crea->name = my_strdup(g_creatures[rnd].name)) == NULL)
    {
      free(crea);
      return (NULL);
    }
  crea->lvl = g_creatures[rnd].lvl;
  crea->pv = g_creatures[rnd].pv;
  crea->pvmax = g_creatures[rnd].pvmax;
  crea->pm = g_creatures[rnd].pm;
  crea->pmmax = g_creatures[rnd].pmmax;
  crea->wild = TRUE;
  return (crea);
}

void	heal_creature(t_creature *creature)
{
  creature->pv = creature->pvmax;
  creature->pm = creature->pmmax;
}

void	up_creature(t_creature *creature)
{
  creature->pvmax *= 2;
  creature->pmmax *= 2;
  creature->wild = FALSE;
}
