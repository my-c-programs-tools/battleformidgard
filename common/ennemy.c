/*
** ennemy.c for sta in /home/jordan/ETNA/DEVC/Battle_For_Midgard
**
** Made by VEROVE Jordan
** Login   <verove_j@etna-alternance.net>
**
** Started on  Wed Dec  7 11:26:15 2016 VEROVE Jordan
** Last update Thu Oct 17 13:36:07 2019 VEROVE Jordan
*/

#include <stdlib.h>
#include "common.h"

t_creature		*current_ennemy(int status)
{
  static t_creature	*creature = NULL;

  if (status == CREATE_NMY)
    {
      if ((creature = get_creature()) == NULL)
	return (NULL);
      servant_speaking();
      my_putstr(CREATURE_SPAWN);
      my_putstr("\t");
      display_creature_info(creature);
      return (creature);
    }
  else if (status == GET_NMY)
    return (creature);
  else if (status == DELETE_NMY)
    {
      delete_creature(creature);
      creature = NULL;
      return (NULL);
    }
  else if (status == RESET_NMY)
    creature = NULL;
  return (NULL);
}
