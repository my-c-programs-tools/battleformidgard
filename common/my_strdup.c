#include <stdlib.h>
#include "common.h"

char	*my_strdup(char *str)
{
  char	*new;
  int	i;

  if ((new = malloc((my_strlen(str) + 1) * sizeof(*new))) == NULL)
    return (my_null_error(MALLOC_ERROR));
  i = 0;
  while (str[i])
    {
      new[i] = str[i];
      ++i;
    }
  new[i] = '\0';
  return (new);
}
