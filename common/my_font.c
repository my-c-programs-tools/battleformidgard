#include <unistd.h>
#include "common.h"

static char	*g_font_scope = RESET_FONT;

void	set_font(char *font)
{
  if (!font)
    return ;
  g_font_scope = font;
  write(1, font, 4);
}

void	set_temporary_font(char *font)
{
  if (!font)
    return ;
  write(1, font, 4);
}

void	set_font_scope()
{
  write(1, g_font_scope, my_strlen(g_font_scope));
}

void	reset_font()
{
  g_font_scope = RESET_FONT;
  write(1, RESET_FONT, 4);
  set_color_scope();
}

int	is_font_blank()
{
  return (!my_strcmp(g_font_scope, RESET_FONT));
}
