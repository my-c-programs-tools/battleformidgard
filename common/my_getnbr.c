int	my_getnbr(char *str)
{
  int	nb;
  int	sign;

  sign = 1;
  nb = 0;
  while (*str && (*str == '-' || *str == '+'))
    sign *= (*(str++) == '+') ? (1) : (-1);
  while (*str && *str >= '0' && *str <= '9')
    nb = (nb * 10) + (*(str++) - '0');
  return (nb * sign);
}
