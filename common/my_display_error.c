#include <unistd.h>
#include <stdlib.h>
#include "common.h"

int	my_int_error(char *error, int ret)
{
  write(2, error, my_strlen(error));
  return (ret);
}

void	*my_null_error(char *error)
{
  write(2, error, my_strlen(error));
  return (NULL);
}
