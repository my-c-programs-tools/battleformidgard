#include <stdlib.h>
#include "common.h"

static int	set_player_name(t_args*, int, char**, int*);

static t_option	g_options[] =
  {
    {'n', "name", &set_player_name},
    {0, NULL, NULL}
  };

static char	*get_opt(char *opt, int *multiple)
{
  if (multiple && opt && opt[0] == '-' && opt[1] != '-')
    *multiple = TRUE;
  else if (multiple)
    *multiple = FALSE;
  while (*opt && *opt == '-')
    ++opt;
  return (opt);
}

static int	my_option_error(char *msg, char *obj)
{
  set_color(RED);
  set_font(BOLD);
  my_putstr(msg);
  if (obj)
    {
      my_putstr(" (");
      my_display(obj, YELLOW, BOLD);
      my_putstr(")");
    }
  my_putstr("\n\n");
  reset_color();
  reset_font();
  my_display(USAGE, YELLOW, NULL);
  return (ERROR);
}

static int	set_player_name(t_args *options, int argc, char **argv, int *i)
{
  if (!argv[*i + 1])
    return (my_option_error(MISSING_PARAM, get_opt(argv[*i], NULL)));
  if ((*i + 1) < argc && (options->name = my_strdup(argv[*i + 1])) == NULL)
    return (ERROR);
  argv[*i + 1] = NULL;
  ++(*i);
  return (SUCCESS);
}

int	get_args(t_args *options, int argc, char **argv)
{
  int	i;
  int	n;
  char	*opt;
  int	multiple;
  int	ret;

  i = 1;
  while (i < argc)
    {
      n = 0;
      ret = FALSE;
      opt = get_opt(argv[i], &multiple);
      while (g_options[n].long_opt && (!ret || multiple) && ret != ERROR)
	{
	  if ((multiple && my_strpos(opt, 0, g_options[n].short_opt) != -1) ||
	      (!multiple && !my_strcasecmp(opt, g_options[n].long_opt)))
	    ret = g_options[n].func(options, argc, argv, &i);
	  ++n;
	}
      if (ret == ERROR || !ret)
	return ((ret == ERROR) ? (ERROR) : (my_option_error(INVALID_OPTION, opt)));
      ++i;
    }
  return (SUCCESS);
}
