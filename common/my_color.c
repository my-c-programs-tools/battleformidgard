#include <unistd.h>
#include "common.h"

static char	*g_color_scope = RESET_COLOR;

void	set_color(char *color)
{
  if (!color)
    return ;
  g_color_scope = color;
  write(1, color, 5);
}

void	set_temporary_color(char *color)
{
  if (!color)
    return ;
  write(1, color, 5);
}

void	set_color_scope()
{
  write(1, g_color_scope, my_strlen(g_color_scope));
}

void	reset_color()
{
  g_color_scope = RESET_COLOR;
  write(1, RESET_COLOR, 4);
  set_font_scope();
}

int	is_color_blank()
{
  return (!my_strcmp(g_color_scope, RESET_COLOR));
}
