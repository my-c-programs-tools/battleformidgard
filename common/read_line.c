#include <stdlib.h>
#include <unistd.h>
#include "common.h"

char		*read_line(char *action)
{
  ssize_t	ret;
  char		*buff;

  display_prompt(action, FALSE);
  if ((buff = malloc(sizeof(*buff) * (50 + 1))) == NULL)
    return (my_null_error(MALLOC_ERROR));
  ret = read(0, buff, 50);
  if (ret > 0)
    {
      buff[ret - 1] = '\0';
      return (buff);
    }
  free(buff);
  if (ret < 0)
    return (my_null_error(READ_ERROR));
  else
    return (my_null_error(READ_END));
}
