#ifndef			_COMMON_H_
# define		_COMMON_H_

/*
** Structures
*/

typedef struct		s_creature
{
  char			*name;
  int			lvl;
  int			pv;
  int			pvmax;
  int			pm;
  int			pmmax;
  int			wild;
}			t_creature;

typedef struct		s_args
{
  char			*name;
}			t_args;

typedef struct		s_option
{
  char			short_opt;
  char			*long_opt;
  int			(*func)(t_args*, int, char**, int*);
}			t_option;

typedef struct		s_commands
{
  char			*short_name;
  char			*long_name;
  int			(*func)();
}			t_commands;

/*
** Ressources
*/

# define		CLEAR		"\033[H\033[2J"

# define		RED		"\033[31m"
# define		GREEN		"\033[32m"
# define		YELLOW		"\033[33m"
# define		BLUE		"\033[34m"
# define		PURPLE		"\033[35m"
# define		CYAN		"\033[36m"
# define		WHITE		"\033[37m"
# define		RESET_COLOR	"\033[0m"

# define		BOLD		"\033[1m"
# define		UNDERLINED	"\033[4m"
# define		RESET_FONT	"\033[0m"

# define		CHECKED_BOX	"  [x] "
# define		UNCHECKED_BOX	"  [ ] "
# define		LOWER_BOX	"  [-] "
# define		UPPER_BOX	"  [+] "
# define		EQUAL_BOX	"  [=] "

# define		TRUE		1
# define		FALSE		0

# define		ENABLE		1
# define		CHECK		0
# define		DISABLE		-1

# define	        SUCCESS		TRUE
# define		FAILURE		FALSE
# define		ERROR		-1

# define		NO_EFFECT_CMD	2
# define	        BAD_USER_CMD	-2

# define		CREATE_NMY	0
# define	        GET_NMY		1
# define	        DELETE_NMY	2
# define	        RESET_NMY	3

# define		NB_CREATURE	5

# define		ABS(x)		((x < 0) ? (-x) : (x))

/*
** Configuration
**
** - For Fonts and Colors, set to 'NULL' if you
**   want the shell's default values
**
** - Don't use NULL for other configurations (such as
**   messages, prompt, etc...) just use an empty
**   string ("")
**
** - Don't use 0 for integer configurations,
**   keep only strictly positive integers
**
*/

# define		SERVANT_NAME	"Grivax"

# define		BFR_PRMPT	"| "
# define		BFR_PRMPT_COLOR	BLUE
# define		BFR_PRMPT_FONT	BOLD

# define		PRMPT_COLOR	YELLOW
# define		PRMPT_FONT	BOLD

# define		AFR_PRMPT	" |~> "
# define		AFR_PRMPT_COLOR	BLUE
# define		AFR_PRMPT_FONT	BOLD

# define		DFLT_PLYR_NAME	"Master"

/*
** Messages
*/

# define		SRVT_WELCOME	"Welcome to Midgard my Lord ! I'm "
# define		SRVT_GOAL	", I'll be your servant through\
 your journey and until my death\n\n"

# define		MALLOC_ERROR	"[FATAL ERROR] Out of memory\n"
# define		READ_ERROR	"[FATAL ERROR] Read error\n"
# define		READ_END	"Reaching end of read\n"

# define		MISSING_PARAM	"Parameter is missing."
# define		INVALID_OPTION	"Invalid option."
# define		USAGE		"Usage:\t./sta [OPTIONS]\n\n-n, --name\
 <string>\tSpecifies player's name (between 3 and 20 characters)\n"

# define		ASK_LVNG	"Do you want to leave ?"
# define		ASK_LVNG_MAN	"enter (Y)es or (N)o"
# define		ASK_LVNG_ERROR	"Wrong format, please try again"

# define		ARMY_GIVE_UP	"Fed up, by your inactivity, your \
whole army left ... You really sucks at plunging worlds into darkness\n"

# define		CREATURE_SPAWN	"A wild Pokem...creature appears !\n\n"
# define		CREATURE_LVL	"Level"
# define		CREATURE_PV	"Life"
# define	        CREATURE_PM	"Moves"

/*
** ask.c
*/
int			ask_leaving();

/*
** creature.c
*/
void			delete_creature(t_creature*);
t_creature		*get_creature();
void			heal_creature(t_creature*);
void			up_creature(t_creature*);

/*
** display_info.c
*/
void			display_creature_info(t_creature*);

/*
** ennemy.c
*/
t_creature		*current_ennemy(int);

/*
** display_instructions.c
*/
void			servant_speaking();
void			display_instruction(char*, char*);
void			display_error_instruction(char*, char*);
void			display_error_char(char*, char);

/*
** my_cmp.c
*/
int			my_strcmp(char*, char*);
int			my_strncmp(char*, char*, int);
int			my_strcasecmp(char*, char*);

/*
** my_color.c
*/
void			set_font(char*);
void			set_temporary_font(char*);
void			set_font_scope();
void			reset_font();
int			is_font_blank();

/*
** my_display.c
*/
int			checking(int);
void			my_putstr(char*);
void			my_display(char*, char*, char*);
void			display_prompt(char*, int);
void			my_fill_display(int, int);

/*
** my_display_error.c
*/
int			my_int_error(char*, int);
void			*my_null_error(char*);

/*
** my_display_nbr.c
*/
void			my_putnbr(int);
void			my_display_nbr(int, char*, char*);

/*
** my_font.c
*/
void			set_color(char*);
void			set_temporary_color(char*);
void			set_color_scope();
void			reset_color();
int			is_color_blank();

/*
** my_getnbr.c
*/
int			my_getnbr(char*);

/*
** my_strdup.c
*/
char			*my_strdup(char*);

/*
** my_strlen.c
*/
int			my_strlen(char*);

/*
** my_strpos.c
*/
int			my_strpos(char*, int, char);

/*
** options.c
*/
int			get_args(t_args*, int, char**);

/*
** read_line.c
*/
char			*read_line(char*);

#endif			/* !_COMMON_H_ */
