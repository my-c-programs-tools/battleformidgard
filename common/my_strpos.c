#include <stdio.h>
#include "common.h"

int	my_strpos(char *str, int offset, char c)
{
  if (!str)
    return (0);
  while (str[offset] && str[offset] != c)
    ++offset;
  if (str[offset] == c)
    return (offset);
  return (-1);
}
