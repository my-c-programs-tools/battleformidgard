#include <unistd.h>
#include <stdlib.h>
#include "common.h"

void	my_putstr(char *str)
{
  write(1, str, my_strlen(str));
}

int             checking(int status)
{
  static int    checking = DISABLE;

  if (status == CHECK)
    return (checking);
  else
    checking = status;
  return (status);
}

void	my_display(char *str, char *color, char *font)
{
  set_temporary_color(color);
  set_temporary_font(font);
  my_putstr(str);
  if (is_color_blank() && is_font_blank())
    set_color_scope();
  else if (is_color_blank())
    {
      set_color_scope();
      set_font_scope();
    }
  else
    {
      set_font_scope();
      set_color_scope();
    }
}

void		display_prompt(char *cmd, int save)
{
  static char	*player_name = NULL;

  if (save)
    player_name = cmd;
  else
    {
      my_display(BFR_PRMPT, BFR_PRMPT_COLOR, BFR_PRMPT_FONT);
      if (!cmd)
	my_display(player_name, CYAN, BOLD);
      else
	my_display(cmd, YELLOW, BOLD);
      my_display(AFR_PRMPT, AFR_PRMPT_COLOR, AFR_PRMPT_FONT);
    }
}

void	my_fill_display(int end, int size)
{
  while (end < size)
    {
      my_putstr(" ");
      ++end;
    }
}
