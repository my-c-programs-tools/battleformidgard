#include <unistd.h>
#include <stdlib.h>
#include "common.h"

static void	rec_aff(int nb)
{
  char		c;

  if (nb > 9 || nb < -9)
    rec_aff(nb / 10);
  c = ABS((nb % 10)) + '0';
  write(1, &c, 1);
}

void	my_putnbr(int nb)
{
  if (nb < 0)
    my_putstr("-");
  rec_aff(nb);
}

void	my_display_nbr(int nb, char *color, char *font)
{
  set_temporary_color(color);
  set_temporary_font(font);
  my_putnbr(nb);
  set_color_scope();
  set_font_scope();
}
