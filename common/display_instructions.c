#include <unistd.h>
#include <stdlib.h>
#include "common.h"

void	servant_speaking()
{
  my_display("\n ** ", YELLOW, BOLD);
  my_display(SERVANT_NAME, CYAN, UNDERLINED);
  my_display(" ** ", YELLOW, BOLD);
  my_putstr("||=> ");
}

void	display_instruction(char *msg, char *man)
{
  servant_speaking();
  my_display(msg, NULL, BOLD);
  my_putstr(" (");
  my_display(man, YELLOW, BOLD);
  my_putstr(")\n\n");
}

void	display_error_instruction(char *error, char *man)
{
  set_color(RED);
  servant_speaking();
  my_display(error, NULL, BOLD);
  my_putstr(" (");
  my_display(man, YELLOW, BOLD);
  my_putstr(")\n\n");
  reset_color();
}

void	display_error_char(char *error, char man)
{
  set_color(RED);
  servant_speaking();
  my_display(error, NULL, BOLD);
  my_putstr(" (");
  set_color(YELLOW);
  set_font(BOLD);
  write(1, &man, 1);
  reset_font();
  set_color(RED);
  my_putstr(")\n");
  reset_color();
}
